﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace gridviewToChart
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                dataGridView1.Rows.Add("alex stewart", 24, 56);
                dataGridView1.Rows.Add("chris harris", 34, 76);
                dataGridView1.Rows.Add("frank smith", 21, 46);
                dataGridView1.Rows.Add("henry paul", 27, 66);
                dataGridView1.Rows.Add("lan bishop", 31, 59);
                for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
                {
                    this.chart1.Series["StudentScore"].Points.AddXY(dataGridView1.Rows[i].Cells[0].Value.ToString(), Convert.ToInt32(dataGridView1.Rows[i].Cells[2].Value.ToString()));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Bir şeyler ters gitti...");
            }
        }

       
    }
}
